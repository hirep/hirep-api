'use strict';
import Knex = require('knex');

export class SubMenuItemModels {
  listall(knex: Knex) {
    return knex('rep_sub_menu_item')
      .orderBy('sub_item_id', 'DESC');
  }

  selectsub(knex: Knex) {
    return knex.select('main_query_id').from('rep_sub_menu_item')

  }

  select(knex: Knex, data: any) {
    // return knex.select('item_id', 'sub_item_name', 'query_sql', 'query_params', 'column_selected', 'template', 'comment', 'sub_item_status', 'level_id', ).from('rep_sub_menu_item')
    return knex('rep_sub_menu_item')
      .whereNotIn('sub_item_id', data)

  }

  insert(knex: Knex, data: any) {
    return knex('rep_sub_menu_item')
      .insert(data);

  }

  listone(knex: Knex, sub_item_id: any) {
    return knex('rep_sub_menu_item')
      .where('sub_item_id', sub_item_id);

  }
  listtwo(knex: Knex, item_id: any, userLevel: any) {
    return knex('rep_sub_menu_item')
      .whereIn('item_id', item_id)
      .andWhere(function () {
        this.where('level_id', '<=', userLevel)
          .orWhere(function () {
            this.orWhere('level_id', '=', '0')
              .orWhere('level_id', '=', '')
              .orWhereNull('level_id')
          })
      })
  }

  add(knex: Knex, data: any) {
    return knex('rep_sub_menu_item')
      .insert(data);
  }

  update(knex: Knex, sub_item_id: any, data: any) {
    return knex('rep_sub_menu_item')
      .where('sub_item_id', sub_item_id)
      .update(data);
  }

  del(knex: Knex, sub_item_id: any) {
    return knex('rep_sub_menu_item')
      .where('sub_item_id', sub_item_id)
      .del();
  }

}