'use strict';

import * as express from 'express';
const router = express.Router();

import { CgroupdisModels } from '../models/cgroup298disease';
const cgroupdisModels = new CgroupdisModels();

router.get('/', (req, res, next) => {
    let db = req.db2;
  
    cgroupdisModels.listall(db)
      .then((results: any) => {
        res.send({ ok: true, rows: results });
      })
      .catch(error => {
        res.send({ ok: false, error: error })
      })
      .finally(() => {
        db.destroy();
      })
  });
  export default router;