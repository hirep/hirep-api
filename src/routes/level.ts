'use strict';

import * as express from 'express';
const router = express.Router();

import { LevelModels } from '../models/level';
const levelModels = new LevelModels();


/* GET home page. */
router.get('/', (req, res, next) => {
    let db = req.db;

    levelModels.listall(db)
        .then((results: any) => {
            res.send({ ok: true, rows: results });
        })
        .catch(error => {
            res.send({ ok: false, error: error })
        })
        .finally(() => {
            db.destroy();
        })
});

router.post('/', (req, res, next) => {
    let db = req.db;

    let level_id = req.body.level_id;
    let level_name = req.body.level_name;
    let comment = req.body.comment;
    let datas: any = {
        level_id: level_id,
        level_name: level_name,
        comment: comment,
    }

    console.log(datas);

    levelModels.add(db, datas)
        .then((results: any) => {
            res.send({ ok: true, rows: results });
        })
        .catch(error => {
            res.send({ ok: false, error: error });
        })
        .finally(() => {
            db.destroy();
        })
})

router.put('/', (req, res, next) => {
    let db = req.db;

    let level_id = req.body.level_id;
    let level_name = req.body.level_name;
    let comment = req.body.comment;
    let datas: any = {
        level_id: level_id,
        level_name: level_name,
        comment: comment,
    }

    console.log(datas);

    levelModels.update(db, level_id, datas)
        .then((results: any) => {
            res.send({ ok: true, rows: results });
        })
        .catch(error => {
            res.send({ ok: false, error: error });
        })
        .finally(() => {
            db.destroy();
        })
})
router.post('/del', (req, res, next) => {
    let db = req.db;

    let level_id = req.body.level_id;

    levelModels.del(db, level_id)
        .then((results: any) => {
            res.send({ ok: true, rows: results });
        })
        .catch(error => {
            res.send({ ok: false, error: error });
        })
        .finally(() => {
            db.destroy();
        })
})


export default router;