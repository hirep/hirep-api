'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class SubMenuItemModels {
    listall(knex) {
        return knex('rep_sub_menu_item')
            .orderBy('sub_item_id', 'DESC');
    }
    selectsub(knex) {
        return knex.select('main_query_id').from('rep_sub_menu_item');
    }
    select(knex, data) {
        return knex('rep_sub_menu_item')
            .whereNotIn('sub_item_id', data);
    }
    insert(knex, data) {
        return knex('rep_sub_menu_item')
            .insert(data);
    }
    listone(knex, sub_item_id) {
        return knex('rep_sub_menu_item')
            .where('sub_item_id', sub_item_id);
    }
    listtwo(knex, item_id, userLevel) {
        return knex('rep_sub_menu_item')
            .whereIn('item_id', item_id)
            .andWhere(function () {
            this.where('level_id', '<=', userLevel)
                .orWhere(function () {
                this.orWhere('level_id', '=', '0')
                    .orWhere('level_id', '=', '')
                    .orWhereNull('level_id');
            });
        });
    }
    add(knex, data) {
        return knex('rep_sub_menu_item')
            .insert(data);
    }
    update(knex, sub_item_id, data) {
        return knex('rep_sub_menu_item')
            .where('sub_item_id', sub_item_id)
            .update(data);
    }
    del(knex, sub_item_id) {
        return knex('rep_sub_menu_item')
            .where('sub_item_id', sub_item_id)
            .del();
    }
}
exports.SubMenuItemModels = SubMenuItemModels;
//# sourceMappingURL=sub_item.js.map