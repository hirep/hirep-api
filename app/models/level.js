'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class LevelModels {
    listall(knex) {
        return knex('rep_levels');
    }
    add(knex, data) {
        return knex('rep_levels')
            .insert(data);
    }
    update(knex, level_id, data) {
        return knex('rep_levels')
            .where('level_id', level_id)
            .update(data);
    }
    del(knex, level_id) {
        return knex('rep_levels')
            .where('level_id', level_id)
            .del();
    }
}
exports.LevelModels = LevelModels;
//# sourceMappingURL=level.js.map