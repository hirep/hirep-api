'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class ReptodayModels {
    bed(knex) {
        let sql = `select 
        
        date_format(now(),'%H') as regtime
         
        
        ,d.nameidpm as ward_name
        ,d.bed as ward_bed
        ,COUNT(DISTINCT an) as ward_pt
         
         
        from 
        hi.ipt as i 
        left join hi.idpm as d on i.ward=d.idpm
        join hi.setup as s 
        where i.dchtype = 0 and year(i.rgtdate) between year(SUBDATE(now(),INTERVAL 1 year)) and year(now())   and ward <> ''
        group by i.ward`;
        return knex.raw(sql);
    }
    today(knex) {
        let sql = `SELECT 
        'total is today' as  today,count(ovst.vn) as amount 
        FROM cln INNER JOIN ovst ON cln.cln = ovst.cln 
        WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d')`;
        return knex.raw(sql);
    }
    todayipt(knex) {
        let sql = `
        SELECT
        'total is today' as  namecln,
        COUNT(pt.male) as male
        FROM hi.ipt 
        LEFT JOIN hi.pt ON ipt.hn = pt.hn
        WHERE dchdate ='0000-00-00' AND vn > 0 AND DATE_FORMAT(ipt.rgtdate,'%Y')>='2017'
        ORDER BY ipt.rgtdate DESC
    `;
        return knex.raw(sql);
    }
    todaytype(knex) {
        let sql = `
        SELECT @rownum:= @rownum+1 as Num, d.* FROM
        (SELECT  
        pttype.namepttype,
        a.pttype,
        pttype.inscl,
        sum(a.cns) as amount
        from(SELECT ovst.vn, 
        ovst.vstdttm, 
        ovst.hn, 
        count(ovst.vn) AS cns, 
        ovst.cln, 
        cln.namecln, 
        ovst.pttype
        FROM cln INNER JOIN ovst ON cln.cln = ovst.cln
        WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d')
        GROUP BY ovst.vn) as a 
        INNER JOIN pttype on pttype.pttype = a.pttype   
        GROUP BY   a.pttype   ORDER BY  amount desc)d, (SELECT @rownum:=0) r 
        LIMIT 0, 1000000 ; 
                        `;
        return knex.raw(sql);
    }
    todaytotal(knex) {
        let sql = `
                        SELECT @rownum:= @rownum+1 as Num, d.* FROM
						(SELECT  a.namecln ,a.cln,sum(a.cns) as amount
                        from(
                        SELECT ovst.vn, 
                        ovst.vstdttm, 
                        ovst.hn,
                        count(ovst.vn) as cns, 
                        ovst.cln, 
                        cln.namecln
                        FROM cln INNER JOIN ovst ON cln.cln = ovst.cln
                        WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') 
                        GROUP by ovst.vn) as a GROUP BY a.cln 
						ORDER BY amount desc)d, (SELECT @rownum:=0) r
                        LIMIT 0, 1000000 ;  
                        `;
        return knex.raw(sql);
    }
    revier(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
                            (SELECT 
                            monthname(o.vstdttm) as m,
                            count(distinct o.vn) as revisit
                            from hi.ovst as o
                            inner join hi.ovstdx as d1 on o.vn=d1.vn
                            inner join 
                            (SELECT hn,vstdttm,icd10
                            from hi.ovst 
                            inner join hi.ovstdx on ovst.vn=ovstdx.vn and cnt='1' where 
                            vstdttm between '2017-10-01 00:00:00' and '2018-09-30 23:59:59' and cln='20100'
                            GROUP BY hn
                            ) as d2 
                            on (TIMEstampDIFF(day,d2.vstdttm,o.vstdttm) between 0 and 1) 
                            and o.vstdttm!=d2.vstdttm 
                            and o.vstdttm>d2.vstdttm 
                            and o.hn=d2.hn 
                            and d1.cnt='1'
                            where 
                            o.vstdttm between '2017-10-01 00:00:00' and '2018-09-30 23:59:59' and o.cln='20100' and d1.icd10 = d2.icd10
                            group by m order by o.vstdttm) d, (SELECT @rownum:=0) r
                            LIMIT 0, 1000000 ; 
                        `;
        return knex.raw(sql);
    }
    opicdtm(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
                            (SELECT 
                            ovstdx.icd10 as ICD10,
                            icd101.icd10name as ICD10NAME,
                            COUNT(icd101.icd10) as Total
                            FROM ovstdx 
                            INNER JOIN icd101 ON icd101.icd10 = ovstdx.icd10 
                            INNER JOIN ovst ON ovst.vn = ovstdx.vn 
                            WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') 
                            AND
                            ovstdx.icd10 NOT LIKE 'Z%' AND
                            ovstdx.icd10 NOT BETWEEN 'O80' AND 'O849'
                            AND ovst.cln!='20100'AND cln!='40100' 
                            GROUP BY icd101.icd10 
                            ORDER BY total DESC
                            LIMIT 0, 10000000) d, (SELECT @rownum:=0) r
                            LIMIT 20 ; 
                        `;
        return knex.raw(sql);
    }
    ericdtm(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
                            (SELECT 
                            ovstdx.icd10 as ICD10,
                            icd101.icd10name as ICD10NAME,
                            COUNT(icd101.icd10) as Total
                            FROM ovstdx 
                            INNER JOIN icd101 ON icd101.icd10 = ovstdx.icd10 
                            INNER JOIN ovst ON ovst.vn = ovstdx.vn 
                            WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') 
                            AND
                            ovstdx.icd10 NOT LIKE 'Z%' AND
                            ovstdx.icd10 NOT BETWEEN 'O80' AND 'O849'
                            AND ovst.cln='20100' 
                            GROUP BY icd101.icd10 
                            ORDER BY total DESC
                            LIMIT 0, 10000000) d, (SELECT @rownum:=0) r
                            LIMIT 0, 20 ; 
                        `;
        return knex.raw(sql);
    }
    dticdtm(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
                            (SELECT 
														dtdx.icdda as ICD10,
                            icdda.nameicdda as ICD10NAME,
                            COUNT(dtdx.icdda) as Total
                            FROM dtdx 
														INNER JOIN dt ON dt.dn = dtdx.dn
                            INNER JOIN icdda ON icdda.codeicdda = dtdx.icdda 
                            WHERE DATE_FORMAT(dt.vstdttm,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d')AND
                            dtdx.icdda NOT LIKE 'Z%'
                            GROUP BY dtdx.icdda
                            ORDER BY total DESC
                            LIMIT 0, 10000000) d, (SELECT @rownum:=0) r
                            LIMIT 0, 1000000 ; 
                        `;
        return knex.raw(sql);
    }
    iptnum(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
							(SELECT aa.dct,aa.name,SUM(aa.10) as 'Oct',SUM(aa.11) as 'Nov',
                            SUM(aa.12)as 'Dec',SUM(aa.01)as 'Jan',SUM(aa.02) as 'Feb',
                            SUM(aa.03)as 'Mar',SUM(aa.04)as 'Apr',SUM(aa.05)as 'May',
                            SUM(aa.06)as 'jun',SUM(aa.07)as 'jul',SUM(aa.08)as 'Aug',
                            SUM(aa.09)as 'Sep',SUM(aa.vn) AS Sum ,SUM(aa.hn) AS hn
                            FROM (
                            SELECT
                            count(ovst.vn) AS vn ,
                            count(DISTINCT(ovst.hn)) AS hn ,
                            dct.lcno AS dct,CONCAT(dct.fname,' ',dct.lname) AS 'name',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 10 THEN '10' END) AS '10',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 11 THEN '11' END) AS '11',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 12 THEN '12' END) AS '12',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 01 THEN '01' END) AS '01',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 02 THEN '02' END) AS '02',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 03 THEN '03' END) AS '03',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 04 THEN '04' END) AS '04',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 05 THEN '05' END) AS '05',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 06 THEN '06' END) AS '06',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 07 THEN '07' END) AS '07',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 08 THEN '08' END) AS '08',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 09 THEN '09' END) AS '09'
                            FROM dct 
                            INNER JOIN ovst ON dct.dct = substr(ovst.dct,3,2)  
                            WHERE dct.lcno !='' AND ovst.dct !='' AND ovst.dct IS NOT NULL AND LEFT(ovst.dct,1) REGEXP '[a-z]' 
                            AND DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') BETWEEN '2017-10-01' AND '2018-09-330'  AND ovst.ovstost= '4'
                            GROUP BY dct 

                            UNION

                            SELECT 
                            count(ovst.vn) AS vn ,
                            count(DISTINCT(ovst.hn)) AS hn ,
                            dct.lcno AS dct,CONCAT(dct.fname,' ',dct.lname) AS 'name',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 10 THEN '10' END) AS '10',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 11 THEN '11' END) AS '11',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 12 THEN '12' END) AS '12',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 01 THEN '01' END) AS '01',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 02 THEN '02' END) AS '02',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 03 THEN '03' END) AS '03',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 04 THEN '04' END) AS '04',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 05 THEN '05' END) AS '05',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 06 THEN '06' END) AS '06',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 07 THEN '07' END) AS '07',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 08 THEN '08' END) AS '08',
                            COUNT(CASE WHEN MONTH(ovst.vstdttm) = 09 THEN '09' END) AS '09'
                            FROM ovst 
                            INNER JOIN dct ON dct.lcno = ovst.dct 
                            WHERE dct.lcno !='' AND ovst.dct !='' AND ovst.dct IS NOT NULL AND (DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') BETWEEN '2017-10-01' AND '2018-09-330') AND ovst.ovstost= '4' 
                            GROUP BY dct 
                            ) AS aa
                            GROUP BY aa.dct ORDER BY sum DESC)d, (SELECT @rownum:=0) r
                            LIMIT 0, 1000000 ; 
                        `;
        return knex.raw(sql);
    }
    reopuc(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
							(SELECT 
                            monthname(a.vstdttm) as 'month',
                            COUNT(a.vn) as 'count'
                            from(SELECT ovst.vn as vn,
                                ovst.an as an, 
                                ovst.vstdttm, 
                                ovst.hn, 
                                ovst.cln, 
                                cln.namecln, 
                                ovst.pttype
                            FROM cln 
                            INNER JOIN ovst ON cln.cln = ovst.cln
                            WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') BETWEEN '2017-10-01' and '2018-09-30' AND ovst.ovstost = '3' AND ovst.an = '0'
                            GROUP BY ovst.vn) as a 
                            INNER JOIN pttype on pttype.pttype = a.pttype
                            WHERE pttype.inscl = 'UCS' 
                            GROUP BY month ORDER BY a.vstdttm,count desc)d, (SELECT @rownum:=0) r
                            LIMIT 0, 1000000 ;   
                        `;
        return knex.raw(sql);
    }
    reipuc(knex) {
        let sql = `
                            SELECT @rownum:= @rownum+1 as Num, d.* FROM
							(SELECT 
                            monthname(a.vstdttm) as 'month',
                            COUNT(a.vn) as 'count'
                            from(SELECT ovst.vn as vn,
                                ovst.an as an, 
                                ovst.vstdttm, 
                                ovst.hn, 
                                ovst.cln, 
                                cln.namecln, 
                                ovst.pttype
                            FROM ovst 
                            INNER JOIN ipt ON ipt.vn = ovst.vn
                            INNER JOIN cln ON cln.cln = ovst.cln
                            WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') BETWEEN '2017-10-01' and '2018-09-30' AND ipt.dchtype = '4' 
                            GROUP BY ovst.vn) as a 
                            INNER JOIN pttype on pttype.pttype = a.pttype
                            WHERE pttype.inscl = 'UCS' 
                            GROUP BY month ORDER BY a.vstdttm,count desc)d, (SELECT @rownum:=0) r
                            LIMIT 0, 1000000 ;  
                        `;
        return knex.raw(sql);
    }
    overvisit(knex) {
        let sql = `
                            SELECT IF(month(o.vstdttm) < 10,year(o.vstdttm), year(ADDDATE(o.vstdttm,INTERVAL 1 year)) ) as yearbudget, 
                            count(distinct case month(o.vstdttm) when 10 then o.vn end) as 'Oct', 
                            count(distinct case month(o.vstdttm) when 11 then o.vn end) as 'Nov', 
                            count(distinct case month(o.vstdttm) when 12 then o.vn end) as 'Dec', 
                            count(distinct case month(o.vstdttm) when 1 then o.vn end) as 'Jan', 
                            count(distinct case month(o.vstdttm) when 2 then o.vn end) as 'Feb', 
                            count(distinct case month(o.vstdttm) when 3 then o.vn end) as 'Mar', 
                            count(distinct case month(o.vstdttm) when 4 then o.vn end) as 'Apr', 
                            count(distinct case month(o.vstdttm) when 5 then o.vn end) as 'May', 
                            count(distinct case month(o.vstdttm) when 6 then o.vn end) as 'jun', 
                            count(distinct case month(o.vstdttm) when 7 then o.vn end) as 'jul', 
                            count(distinct case month(o.vstdttm) when 8 then o.vn end) as 'Aug', 
                            count(distinct case month(o.vstdttm) when 9 then o.vn end) as 'Sep', 
                            count(distinct o.vn) as total ,
                            count(distinct o.hn) as hn 
                            FROM hi.ovst AS o 
                            LEFT JOIN hi.dt as d ON o.vn=d.vn 
                            WHERE (o.dct !='' or d.dnt !='') 
                            GROUP BY yearbudget 
                            DESC LIMIT 5;  
                        `;
        return knex.raw(sql);
    }
    overadmin(knex) {
        let sql = `
                            SELECT IF(month(o.vstdttm) < 10,year(o.vstdttm),year(ADDDATE(o.vstdttm,INTERVAL 1 year))) as yearbudget, 
                            count(distinct case month(o.vstdttm) when 10 then i.vn end) as 'Oct', 
                            count(distinct case month(o.vstdttm) when 11 then i.vn end) as 'Nov', 
                            count(distinct case month(o.vstdttm) when 12 then i.vn end) as 'Dec', 
                            count(distinct case month(o.vstdttm) when 1 then i.vn end) as 'Jan', 
                            count(distinct case month(o.vstdttm) when 2 then i.vn end) as 'Feb', 
                            count(distinct case month(o.vstdttm) when 3 then i.vn end) as 'Mar', 
                            count(distinct case month(o.vstdttm) when 4 then i.vn end) as 'Apr', 
                            count(distinct case month(o.vstdttm) when 5 then i.vn end) as 'May', 
                            count(distinct case month(o.vstdttm) when 6 then i.vn end) as 'jun', 
                            count(distinct case month(o.vstdttm) when 7 then i.vn end) as 'jul', 
                            count(distinct case month(o.vstdttm) when 8 then i.vn end) as 'Aug', 
                            count(distinct case month(o.vstdttm) when 9 then i.vn end) as 'Sep', 
                            count(distinct i.an) as total ,
                            count(distinct o.hn) as hn 
                            FROM hi.ovst AS o 
                            INNER JOIN hi.ipt as i ON o.an=i.an 
                            GROUP BY yearbudget
                            DESC LIMIT 5;  
                        `;
        return knex.raw(sql);
    }
    typetotal(knex, startdate, enddate) {
        let sql = `
                        SELECT @rownum:= @rownum+1 as Num, d.* FROM
						(SELECT  a.namecln,a.cln,sum(a.cns) as cns2
                        from(
                        SELECT ovst.vn, 
                        ovst.vstdttm, 
                        ovst.hn,
                        count(ovst.vn) as cns, 
                        ovst.cln, 
                        cln.namecln
                        FROM cln INNER JOIN ovst ON cln.cln = ovst.cln
                        WHERE DATE(ovst.vstdttm) BETWEEN '2017-10-01' and '2018-09-30'
                        GROUP by ovst.vn) as a GROUP BY a.cln 
						ORDER BY cns2 desc)d, (SELECT @rownum:=0) r
                        LIMIT 0, 1000000 ;  
                        `;
        return knex.raw(sql);
    }
    todayreferout(knex) {
        let sql = `SELECT
        
      a.vsttime  as refer_time,
       cln.namecln  as clinic,
      
      icd101.icd10name ,
       
      hospcode.off_name1  as refer_to_hos
      from
      
      (SELECT orfro.rfrlct, 
          orfro.cln, 
          orfro.rfrcs, 
          orfro.icd10, 
          orfro.vstdate, 
          orfro.vsttime, 
          orfro.ward
      FROM orfro
      WHERE DATE_FORMAT(orfro.vstdate,'%Y-%m-%d')= CURDATE() and orfro.cln is not  null
      ORDER BY orfro.vsttime asc) as a   
      INNER join icd101 on icd101.icd10 = a.icd10
      INNER join cln on cln.cln =a.cln
      inner join  hospcode on hospcode.off_id = a.rfrlct`;
        return knex.raw(sql);
    }
    todayreferback(knex) {
        let sql = `
        SELECT
        time(ovst.vstdttm)  AS refer_time,
        IF(idpm.nameidpm != Null, 'non',idpm.nameidpm) AS dprtm,
        
        icd101.icd10name ,
        hospcode.namehosp AS refer_in_hos 
        FROM
        orfri
        INNER JOIN hospcode ON hospcode.off_id = orfri.rfrlct
        INNER JOIN ovst ON ovst.vn = orfri.vn
        LEFT JOIN ipt ON ipt.vn = ovst.vn
        LEFT JOIN idpm ON idpm.idpm = ipt.ward
        left JOIN icd101 ON icd101.icd10 = ipt.prediag
        where  orfri.accdate  = CURDATE()`;
        return knex.raw(sql);
    }
    todayrefersocial(knex) {
        let sql = `
        SELECT
       
      a.vsttime  as refer_time,
       
      idpm.nameidpm as dprtm,
      icd101.icd10name,
       
      hospcode.off_name1  as refer_so_hos
      from
      
      (SELECT orfro.rfrlct, 
       
          orfro.rfrcs, 
          orfro.icd10, 
          orfro.vstdate, 
          orfro.vsttime, 
          orfro.ward
      FROM orfro
      WHERE DATE_FORMAT(orfro.vstdate,'%Y-%m-%d')= CURDATE()  
      ORDER BY orfro.vsttime asc) as a   
      INNER join icd101 on icd101.icd10 = a.icd10
      INNER join idpm on idpm.idpm =a.ward
      inner join  hospcode on hospcode.off_id = a.rfrlct
      `;
        return knex.raw(sql);
    }
    todayopddead(knex) {
        let sql = `
        SELECT
      
        a.vsttime  as  dead_time,
        a.icd10 ,
        a.icd10name,
        cln.namecln  as  clinic
        
        from
        
        (SELECT ovstost.ovstost, 
            ovstost.nameovstos, 
            DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') AS vstdate, 
            DATE_FORMAT(ovst.vstdttm,'%H:%i:%s') AS vsttime,
            ovst.hn, 
             ovst.cln,
          group_CONCAT(ovstdx.icd10 SEPARATOR ',') AS icd10,
         group_CONCAT(icd101.icd10name SEPARATOR ',') AS icd10name
        FROM 
        ovstost 
        INNER JOIN ovst ON ovstost.ovstost = ovst.ovstost
        INNER JOIN ovstdx ON ovstdx.vn = ovst.vn
        INNER join icd101 on icd101.icd10 = ovstdx.icd10
        WHERE DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') = CURDATE()
        and
        ovstost.ovstost = '2'  
        GROUP by  ovst.vstdttm,ovst.hn) as a 
        
        INNER join cln on cln.cln  =  a.cln  ORDER BY  a.vstdate desc
      `;
        return knex.raw(sql);
    }
    todayipddead(knex) {
        let sql = `
        select
     
        a.dchtime  as dead_time,
        a.icd10,
        a.icd10name,
        a.nameidpm  as dprtm
        
        
        from(SELECT ipt.ward,  
             ipt.an,
            ipt.dchdate, 
        group_CONCAT(iptdx.icd10 SEPARATOR ',') AS icd10,
        GROUP_CONCAT(icd101.icd10name SEPARATOR ',') AS icd10name,
             ipt.dchtime ,
             idpm.nameidpm
             
             
        FROM iptdx INNER JOIN ipt ON iptdx.an = ipt.an
        
        
         INNER JOIN icd101  on icd101.icd10 = iptdx.icd10
         INNER JOIN idpm  on idpm.idpm = ipt.ward
        
        WHERE ipt.dchdate = CURDATE()   and ipt.dchtype in('8','9')
        
         group by ipt.an,ipt.dchdate,ipt.dchtime,ipt.ward) as a  ORDER BY   a.dchdate, a.dchtime  asc
        
      `;
        return knex.raw(sql);
    }
    todaylrbrith(knex) {
        let sql = `
        SELECT
        child1.brthtime,
        lr1.bresult AS icd10,
        icd101.icd10name,
        IF(child1.male='1', "ชาย", "หญิง") AS sex,
        child1.brthwt,
        child1.alive
        FROM
        lr AS lr1
        left JOIN icd101 ON icd101.icd10 = lr1.bresult
        left JOIN child  as child1 ON child1.lrno = lr1.lrno
        where  lr1.brthdate = CURDATE()
        ORDER BY brthtime
      `;
        return knex.raw(sql);
    }
    todayrevisit(knex) {
     /*   let sql = `
        select 
        a.sex,a.age,a.first_dx as dx ,DATE_FORMAT(a.first_visit,'%Y-%m-%d')as fvisit
        ,DATE_FORMAT(a.revisit,'%Y-%m-%d') as revisit,a.revisit_time as revtime
        from(
        select 
        p.hn,
        p.pop_id as cid,
        concat(p.fname,' ',p.lname) as fullname,
        m.namemale as sex,
        floor(datediff(date(o.vstdttm),p.brthdate)/365.25) as age,
        d1.icd10 as first_dx,
        d2.icd10  as secon_dx,
        d2.vstdttm as first_visit,
        o.vstdttm as revisit,
        (TIMEDIFF(o.vstdttm,d2.vstdttm)) as revisit_time,
        p.fdate
        from hi.ovst as o
        inner join hi.pt as p on p.hn=o.hn
        inner join hi.male as m on p.male=m.male
        inner join hi.ovstdx as d1 on o.vn=d1.vn and d1.icd10 not like 'Z%'
        inner join (
        select hn,vstdttm,icd10 
        from hi.ovst 
        inner join hi.ovstdx on ovst.vn=ovstdx.vn and icd10 not like 'Z%' 
        where   date(ovst.vstdttm) between date_add(curdate(),interval -2 day)     
           AND  curdate()
        ) as d2 on (TIMEDIFF(o.vstdttm,d2.vstdttm) between 1 and 475959) and o.hn=d2.hn and d2.icd10=d1.icd10
        where   date(o.vstdttm) between date_add(curdate(),interval -2 day)     
         AND  curdate()  group by first_visit) as a where  date(a.revisit) = CURDATE() and date(a.fdate) <> CURDATE()
      `;
        return knex.raw(sql);
        */
    }
    todayaccident(knex) {
        let sql = `
        SELECT
        
        acci.nameacci  as name_acci,
         a.icd103 as icd10,
         a.icd10name,
        COUNT(a.traffic) AS  amount
        
        from
        (SELECT emergency.vn, 
            emergency.traffic, 
            emergency.vstdttm, 
            ovst.hn, 
              
         group_CONCAT(ovstdx.icd10 SEPARATOR ',') AS icd103,
         group_CONCAT(icd101.icd10name SEPARATOR ',') AS icd10name
        
             
        FROM emergency INNER JOIN ovst ON ovst.vn = emergency.vn AND ovst.vstdttm = emergency.vstdttm
             INNER JOIN acci ON acci.codeacci = emergency.traffic
             INNER JOIN ovstdx ON ovstdx.vn = ovst.vn
              INNER join icd101  on icd101.icd10 = ovstdx.icd10
        WHERE DATE_FORMAT(emergency.vstdttm,'%Y-%m-%d') = CURDATE()   GROUP BY  emergency.traffic,emergency.traffic,emergency.vstdttm,ovst.hn) as a
         INNER join acci on acci.codeacci =a.traffic    GROUP BY  a.traffic
      `;
        return knex.raw(sql);
    }
    todayservicedailly(knex) {
        let sql = `
        SELECT
        (
            CASE
            WHEN time(o.vstdttm) BETWEEN '00:00:00'
            AND '07:59:59' THEN
                '00.00 - 07.59  น.'
            WHEN time(o.vstdttm) BETWEEN '08:00:00'
            AND '15:59:59' THEN
                '08.00 - 15.59  น.'
            WHEN time(o.vstdttm) BETWEEN '16:00:00'
            AND '19:59:59' THEN
                '16.00 - 19.59  น.'
            WHEN time(o.vstdttm) BETWEEN '20:00:00'
            AND '23:59:59' THEN
                '20.00 - 23.59  น.'
            END
        ) AS job,
        Count(
            DISTINCT CASE o.cln
            WHEN '10100' THEN
                o.vn
            END
        ) AS opd,
        Count(
            DISTINCT CASE o.cln
            WHEN '20100' THEN
                o.vn
            END
        ) AS er,
        count(
            DISTINCT CASE
            WHEN icd9cm IN (
                9356,
                9357,
                9359,
                8622,
                8628,
                8659,
                8611,
                8621,
                8669,
                7751,
                2752
            ) THEN
                o.vn
            END
        ) AS proc,
        count(DISTINCT o.an) AS ADMIT,
        count(DISTINCT r.rfrno) AS RE_OUT,
        count(DISTINCT i.rfrno) AS RE_IN
    FROM
        hi.ovst AS o
    LEFT JOIN hi.orfro AS r ON o.vn = r.vn
    LEFT JOIN hi.orfri AS i ON o.vn = i.vn
    LEFT JOIN hi.oprt AS p ON o.vn = p.vn
    AND p.icd9cm IN (
        9356,
        9357,
        9359,
        8622,
        8628,
        8659,
        8611,
        8621,
        8669,
        7751,
        2752
    )
    WHERE
        DATE_FORMAT(o.vstdttm,'%Y-%m-%d') = date(now())
    GROUP BY
        job
    ORDER BY
        o.vstdttm
      `;
        return knex.raw(sql);
    }
    todayadmitted(knex) {
        let sql = `
        Select  idpm.nameidpm as ward
        ,count(case when  rgttime between 0 and 759 then ipt.an end) as midnight
        , count(case when rgttime between 800 and 1559 then ipt.an end) as morning 
        , count(case when rgttime between 1600 and 1959 then ipt.an end) as after1620 
        , count(case when rgttime between 2000 and 2359 then ipt.an end) as afternoo ,
        count(rgttime) as amount
        From hi.ipt
        Inner join hi.idpm on ipt.ward = idpm.idpm
        Where rgtdate = curdate()
        Group by ipt.ward
      `;
        return knex.raw(sql);
    }
    todayor(knex) {
        let sql = `
        SELECT
        a2.nameidpm as ward 
        , count(case when a2.opstptime between '08:00:00' and '15:59:00' then a2.orno end) as in_time  
        ,count(case when  (a2.opstptime Between '16:00:00' and '23:59:00' or a2.opstptime between '00:00:00' and '07:59:00')   then a2.orno end) as out_time
        
       ,count(a2.orno)as amount
               
       from
       (SELECT
       a1.icd9cm,a1.nameidpm,a1.opstpdate,a1.opstptime,a1.ward,cm.icd9name,a1.orno,a1.ward as ward2
       from(SELECT
       date(o.opstpdate) as opstpdate,
       time(o.opstpdate)as opstptime,
       opr.icd9cm,
       i.ward,
       id.nameidpm ,
       o.orno
       FROM
       ors AS o
       INNER JOIN op AS opr ON opr.orno = o.orno
       INNER JOIN ipt AS i ON i.vn = o.vn AND i.an = o.an AND i.hn = o.hn
       INNER JOIN idpm as id ON id.idpm = i.ward
       WHERE
       date(o.opstrdate) = CURDATE()) as a1 INNER JOIN icd9cm2 as cm on cm.icd9cm = a1.icd9cm) a2  GROUP BY a2.ward2
      `;
        return knex.raw(sql);
    }
    todaygrage(knex) {
        let sql = `
        SELECT
        CASE
                WHEN brthdate1 BETWEEN '0' and '14'  THEN '0-14'
                WHEN brthdate1 BETWEEN '15' and '59'  THEN '15-59'
                ELSE '60+' 
               END AS Age_Range,
        SUM(CASE WHEN  aa2.male= '1' THEN 1 ELSE 0 END) AS Males,
        SUM(CASE WHEN  aa2.male= '2' THEN 1 ELSE 0 END) AS Females  
        from(
        SELECT
        DATE_FORMAT(now(),'%Y') - DATE_FORMAT(p.brthdate,'%Y') as brthdate1  ,
        o1.hn,
        p.male
        from(
        SELECT
         o.hn,
        o.vn
        FROM
        ovst AS o where  date(o.vstdttm) = CURDATE())as  o1
        INNER JOIN pt  p ON p.hn = o1.hn
         GROUP BY
         DATE_FORMAT(now(),'%Y') - DATE_FORMAT(p.brthdate,'%Y') ,o1.hn) as aa2
        GROUP BY
         CASE
          WHEN brthdate1 BETWEEN '0' and '14'  THEN '0-14'
          WHEN brthdate1 BETWEEN '15' and '59'  THEN '15-59'
          ELSE '60+'
          END
      `;
        return knex.raw(sql);
    }
}
exports.ReptodayModels = ReptodayModels;
//# sourceMappingURL=reptoday.js.map