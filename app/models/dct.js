'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class DctModels {
    listall(knex) {
        return knex('dct')
            .whereIn('specialty', ['SU', 'ORT', 'EN', 'ANS', 'OB', 'UR'])
            .andWhere('statusdct', '=', '0')
            .andWhere('outdate', '0000-00-00')
            .andWhere('startdate', '0000-00-00');
    }
}
exports.DctModels = DctModels;
//# sourceMappingURL=dct.js.map