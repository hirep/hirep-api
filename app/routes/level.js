'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const level_1 = require("../models/level");
const levelModels = new level_1.LevelModels();
router.get('/', (req, res, next) => {
    let db = req.db;
    levelModels.listall(db)
        .then((results) => {
        res.send({ ok: true, rows: results });
    })
        .catch(error => {
        res.send({ ok: false, error: error });
    })
        .finally(() => {
        db.destroy();
    });
});
router.post('/', (req, res, next) => {
    let db = req.db;
    let level_id = req.body.level_id;
    let level_name = req.body.level_name;
    let comment = req.body.comment;
    let datas = {
        level_id: level_id,
        level_name: level_name,
        comment: comment,
    };
    console.log(datas);
    levelModels.add(db, datas)
        .then((results) => {
        res.send({ ok: true, rows: results });
    })
        .catch(error => {
        res.send({ ok: false, error: error });
    })
        .finally(() => {
        db.destroy();
    });
});
router.put('/', (req, res, next) => {
    let db = req.db;
    let level_id = req.body.level_id;
    let level_name = req.body.level_name;
    let comment = req.body.comment;
    let datas = {
        level_id: level_id,
        level_name: level_name,
        comment: comment,
    };
    console.log(datas);
    levelModels.update(db, level_id, datas)
        .then((results) => {
        res.send({ ok: true, rows: results });
    })
        .catch(error => {
        res.send({ ok: false, error: error });
    })
        .finally(() => {
        db.destroy();
    });
});
router.post('/del', (req, res, next) => {
    let db = req.db;
    let level_id = req.body.level_id;
    levelModels.del(db, level_id)
        .then((results) => {
        res.send({ ok: true, rows: results });
    })
        .catch(error => {
        res.send({ ok: false, error: error });
    })
        .finally(() => {
        db.destroy();
    });
});
exports.default = router;
//# sourceMappingURL=level.js.map