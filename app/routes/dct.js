'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const dct_1 = require("../models/dct");
const dctModels = new dct_1.DctModels();
router.get('/', (req, res, next) => {
    let db = req.db2;
    dctModels.listall(db)
        .then((results) => {
        res.send({ ok: true, rows: results });
    })
        .catch(error => {
        res.send({ ok: false, error: error });
    })
        .finally(() => {
        db.destroy();
    });
});
exports.default = router;
//# sourceMappingURL=dct.js.map