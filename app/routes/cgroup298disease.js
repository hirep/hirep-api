'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const cgroup298disease_1 = require("../models/cgroup298disease");
const cgroupdisModels = new cgroup298disease_1.CgroupdisModels();
router.get('/', (req, res, next) => {
    let db = req.db2;
    cgroupdisModels.listall(db)
        .then((results) => {
        res.send({ ok: true, rows: results });
    })
        .catch(error => {
        res.send({ ok: false, error: error });
    })
        .finally(() => {
        db.destroy();
    });
});
exports.default = router;
//# sourceMappingURL=cgroup298disease.js.map